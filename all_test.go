// Copyright 2024 The libfreetype-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libfreetype // import "modernc.org/libfreetype"

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"testing"
	"unsafe"

	_ "modernc.org/cc/v4"
	_ "modernc.org/ccgo/v4/lib"
	_ "modernc.org/fileutil/ccgo"
	"modernc.org/libc"
	_ "modernc.org/libz"
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	target = fmt.Sprintf("%s/%s", goos, goarch)
)

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

var (
	faceHandle uintptr
	libHandle  uintptr
)

func Test(t *testing.T) {
	tls := libc.NewTLS()

	defer tls.Close()

	rc := XFT_Init_FreeType(tls, uintptr(unsafe.Pointer(&libHandle)))
	if rc != 0 {
		t.Fatalf("FT_Init_FreeType failed: rc=%v", rc)
	}

	pth, err := libc.CString(filepath.Join("testdata", "Go-Regular.ttf"))
	if err != nil {
		t.Fatalf("CString: %v", err)
	}

	defer libc.Xfree(tls, pth)

	rc = XFT_New_Face(tls, libHandle, pth, 0, uintptr(unsafe.Pointer(&faceHandle)))
	if rc != 0 {
		t.Fatalf("FT_New_Face failed: rc=%v", rc)
	}

	face := *(*TFT_FaceRec)(unsafe.Pointer(faceHandle))
	t.Logf("%+v", face)
	if g, e := libc.GoString(face.Ffamily_name), "Go"; g != e {
		t.Fatalf("face.Ffamily_name=%q, expected %q", g, e)
	}

	if g, e := libc.GoString(face.Fstyle_name), "Regular"; g != e {
		t.Fatalf("face.Fstyle_name=%q, expected %q", g, e)
	}
}
