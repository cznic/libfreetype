// Copyright 2024 The libfreetype-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"modernc.org/cc/v4"
	ccgo "modernc.org/ccgo/v4/lib"
	util "modernc.org/fileutil/ccgo"
)

const (
	aname = "objs/.libs/libfreetype.a"
)

var (
	archiveVersion = "2.10.4"
	archivePath    string
	goos           = runtime.GOOS
	goarch         = runtime.GOARCH
	target         = fmt.Sprintf("%s/%s", goos, goarch)
	make           = "make"
	sed            = "sed"
	j              = fmt.Sprint(runtime.GOMAXPROCS(-1))
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintln(os.Stderr, strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	if ccgo.IsExecEnv() {
		if err := ccgo.NewTask(goos, goarch, os.Args, os.Stdout, os.Stderr, nil).Main(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		return
	}

	switch target {
	case "freebsd/amd64", "freebsd/arm64", "openbsd/amd64", "darwin/amd64", "darwin/arm64":
		make = "gmake"
		sed = "gsed"
	}
	switch target {
	case "linux/arm64", "linux/loong64", "linux/386", "linux/arm", "linux/amd64":
		archiveVersion = "2.12.1"
	case "linux/riscv64":
		archiveVersion = "2.13.2"
	}

	archivePath = "freetype-" + archiveVersion + ".tar.gz"
	_, extractedArchivePath := filepath.Split(archivePath)
	extractedArchivePath = extractedArchivePath[:len(extractedArchivePath)-len(".tar.gz")]
	tempDir := os.Getenv("GO_GENERATE_DIR")
	dev := os.Getenv("GO_GENERATE_DEV") != ""
	switch {
	case tempDir != "":
		util.MustShell(true, nil, "sh", "-c", fmt.Sprintf("rm -rf %s", filepath.Join(tempDir, extractedArchivePath)))
	default:
		var err error
		if tempDir, err = os.MkdirTemp("", "libfreetype-generate"); err != nil {
			fail(1, "creating temp dir: %v\n", err)
		}

		defer func() {
			switch os.Getenv("GO_GENERATE_KEEP") {
			case "":
				os.RemoveAll(tempDir)
			default:
				fmt.Printf("%s: temporary directory kept\n", tempDir)
			}
		}()
	}
	libRoot := filepath.Join(tempDir, extractedArchivePath)
	makeRoot := libRoot
	fmt.Fprintf(os.Stderr, "archivePath %s\n", archivePath)
	fmt.Fprintf(os.Stderr, "extractedArchivePath %s\n", extractedArchivePath)
	fmt.Fprintf(os.Stderr, "tempDir %s\n", tempDir)
	fmt.Fprintf(os.Stderr, "libRoot %s\n", libRoot)
	fmt.Fprintf(os.Stderr, "makeRoot %s\n", makeRoot)
	ccgoInc, err := filepath.Abs(filepath.Join(libRoot, "ccgo"))
	fmt.Fprintf(os.Stderr, "includeDir %s\n", ccgoInc)
	if err != nil {
		fail(1, "%s\n", err)
	}

	f, err := os.Open(archivePath)
	if err != nil {
		fail(1, "cannot open tar file: %v\n", err)
	}

	util.MustUntar(true, tempDir, f, nil)
	util.MustCopyFile(true, "LICENSE-FREETYPE", filepath.Join(libRoot, "docs", "FTL.TXT"), nil)
	util.MustCopyDir(true, libRoot, filepath.Join("internal", "overlay", "all"), nil)
	//util.MustCopyDir(true, libRoot, filepath.Join("internal", "overlay", goos, goarch), nil)
	f.Close()
	util.MustCopyDir(true, ccgoInc, filepath.Join("..", "libz", "include", goos, goarch), nil)
	result := "ccgo.go"
	ilibz := filepath.Join(util.MustAbsCwd(true), "..", "libz", "include", goos, goarch)
	util.MustInDir(true, makeRoot, func() (err error) {
		util.MustShell(true, nil, "sh", "-c", "go mod init example.com/libfreetype ; go get modernc.org/libc@latest modernc.org/libz@latest")
		config := []string{os.Args[0]}
		if dev {
			util.MustShell(true, nil, "sh", "-c", "go work init ; go work use $GOPATH/src/modernc.org/libc $GOPATH/src/modernc.org/libz")
			config = append(config,
				"-absolute-paths",
				"-keep-object-files",
				"-positions",
				"-verify-types",
			)
		}
		cflags := []string{"-DFT_CONFIG_OPTION_NO_ASSEMBLER"}
		switch target {
		case "linux/amd64":
			util.MustShell(true, nil, sed, "-i", `s/FT_SSE2 1/FT_SSE2 0/g`, "src/smooth/ftgrays.c")
		}
		m64Double := cc.LongDouble64Flag(goos, goarch)
		if m64Double != "" {
			config = append(config, m64Double)
		}
		util.MustShell(true, nil, "sh", "-c", fmt.Sprintf(`
CFLAGS='%s' ./configure \
	--disable-shared \
	--with-brotli=no \
	--with-bzip2=no \
	--with-harfbuzz=no \
	--with-png=no \
	--with-zlib=yes \
`, //TODO brotli, png
			strings.Join(cflags, " ")),
		)
		config = append(config,
			"--package-name", "libfreetype",
			"--prefix-enumerator=_",
			"--prefix-external=x_",
			"--prefix-field=F",
			"--prefix-macro=m_",
			"--prefix-static-internal=_",
			"--prefix-static-none=_",
			"--prefix-tagged-enum=_",
			"--prefix-tagged-struct=T",
			"--prefix-tagged-union=T",
			"--prefix-typename=T",
			"--prefix-undefined=_",
			"-ignore-unsupported-alignment",
			"-I", ilibz,
		)
		if err := ccgo.NewTask(goos, goarch, append(config, "-exec", make, "-j", j, "library"), os.Stdout, os.Stderr, nil).Exec(); err != nil {
			return err
		}

		if err := ccgo.NewTask(goos, goarch, append(config, "-o", result, aname, "-lz"), os.Stdout, os.Stderr, nil).Main(); err != nil {
			return err
		}

		util.MustShell(true, nil, sed, "-i", `s/\<T__\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/t__\1/g`, result)
		util.MustShell(true, nil, sed, "-i", `s/\<x_\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/X\1/g`, result)
		return nil
	})

	fn := fmt.Sprintf("ccgo_%s_%s.go", goos, goarch)
	util.MustCopyFile(true, fn, filepath.Join(makeRoot, result), nil)
	util.MustCopyDir(false, filepath.Join("include", goos, goarch), filepath.Join(makeRoot, "include"), nil)
}
