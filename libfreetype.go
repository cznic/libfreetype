// Copyright 2024 The libfreytepy-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libfreetype is a ccgo/v4 version of libfreetype.a (FreeType, https://freetype.org/index.html)
//
// # In action
//
//   - [freetype] A Go library that implements much of the FreeType C library
//
// [freetype]: https://github.com/pekim/freetype
package libfreetype // import "modernc.org/libfreetype"
